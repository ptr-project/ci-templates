#!/bin/sh

echo $#
if [ $# -ne 1 ]; then
    echo "Missing filename"
    exit 1
fi

jq --null-input --arg yaml "$(cat $1)" '.content=$yaml' \
    | curl "https://gitlab.com/api/v4/ci/lint/?include_merged_yaml=true" \
    --header 'Content-Type: application/json' --data @- > test
jq --raw-output < test
jq -e '((.errors | length) + (.warnings | length)) == 0' < test
